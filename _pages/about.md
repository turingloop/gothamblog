---
layout: page
title: About
permalink: /about/
---

I spent two years at Kansas State University majoring in Computer Science and Physics. During that time i worked in Dr. Ruth Welti’s Lipidomics lab. Currently finishing up my Bachelors in Computer Science at Northwest Missouri State University. I am interested in Machine Learning, Multi-Agent Systems, Cryptography and Algorithms.

My recent Computer Science courses include Database Systems, Network Fundamentals, and Internet Of Things. My mathematics courses include Calculus I, Calculus II, Probability and Statistics, Applied Matrix Theory, and Discrete Mathematics.

In my free time i like tinkering and learning new things. I like working with Raspberry Pi, learning Rails framework, implementing interesting Data Structures, and teaching myself advanced Math through Coursera.org and Edx.org